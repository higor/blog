class Category < ActiveRecord::Base
  
  has_many :posts
  
  attr_accessible :nome, :slug
  
  validates_presence_of :nome
end
