class Post < ActiveRecord::Base
  
  before_save :set_category
  
  belongs_to :category
  
  attr_accessible :body, :category_id, :titulo
  
  validates_presence_of :titulo, :body
  
  
  def set_category
    if category_id.nil?
      self.category_id = 1
    end
  end
  
end
