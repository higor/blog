class HomeController < ApplicationController
  def index
    @posts = Post.includes(:category).paginate(:page => params[:page], :per_page => 1)
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @posts }
      format.js
    end
    
  end
end
