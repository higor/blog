module ApplicationHelper
  def show_item_url(url)
    link_to raw('<i class="icon-info-sign"></i>'), url, :class => "btn btn-small btn-primary", :tip => "tooltip", :title => "Ver item"
  end
  
  def new_item_url(url)
    link_to raw('<i class="icon-pencil"></i>'), url, :class => "btn btn-small", :tip => "tooltip", :title => "Nova item"
  end
  
  def edit_item_url(url)
    link_to raw('<i class="icon-edit"></i>'), url, :class => "btn btn-small btn-primary", :tip => "tooltip", :title => "Editar item"
  end
  
  def delete_item_url(url)
    link_to raw('<i class="icon-trash"></i>'), 
            url, 
            method: :delete, 
            data: { confirm: 'Are you sure?' },
            remote: true, 
            :class => "btn btn-small btn-primary", 
            :tip => "tooltip", 
            :title => "Deletar item"
  end
  
  def paginate(collection, params= {})
    will_paginate collection, params.merge(:renderer => RemoteLinkPaginationHelper::LinkRenderer)
  end
end
