class CreatePosts < ActiveRecord::Migration
  def up
    create_table :posts do |t|
      t.string :titulo
      t.text :body
      t.integer :category_id

      t.timestamps
    end
    add_index :posts, :category_id
  end
  
  def down
    drop_table :posts
  end
end
